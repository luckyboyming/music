// components/like/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    like: {
      type: Boolean,
      value: false
    },
    count: {
      type: Number
    },
    readOnly: {
      type: Boolean
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    defaultSrc: '../images/like@dis.png',
    currentSrc: '../images/like.png'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onchange(){
      if(this.properties.readOnly) return
      let count = this.properties.count
      let like = this.properties.like
      this.setData({
        like: !like,
        count: like?count-1:count+1
      })
      let setVal = this.data.like ? 'like' : 'cancel'
      this.triggerEvent('isClick' ,{
        typeClick: setVal
      })
    }
  }
})
