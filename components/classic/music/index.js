import { classicBehavior } from '../classic-beh.js'

let mgSrc = wx.getBackgroundAudioManager()

Component({
  /**
   * 组件的属性列表
   */
  behaviors: [classicBehavior ],
  properties: {
    src: String,
    title:String
  },

  /**
   * 组件的初始数据
   */
  data: {
    isPlay: false,
    pauseSrc: '../../images/player@pause.png',
    playSrc: '../../images/player@play.png'
  },
  attached: function(){
    //判定当前音乐是否在播放状态
    this._isPlayState()
    //开发工具那个音频播放器
    this._MonitorSwitch()
  },
  detached:function(){
   // console.log('进入音乐播放器的销毁状态detached')
   // mgSrc.stop()
  },
  /**
   * 组件的方法列表
   */
  methods: {
    _isPlayState(){
      if (mgSrc.paused){
        this.setData({
          isPlay: false
        })
        return
      }
      if(mgSrc.src == this.properties.src){
        this.setData({
          isPlay: true
        })
      }
    },
    onPlay(){
      if(!this.data.isPlay){
        this.setData({
          isPlay: true
        })
        mgSrc.src = this.properties.src
        mgSrc.title = this.properties.title //不能少
      }else{
        this.setData({
          isPlay: false
        })
       mgSrc.pause()
      }
    },
    _MonitorSwitch(){
       mgSrc.onPlay(()=>{
         this._isPlayState()
       })
      mgSrc.onPause(() => {
        this._isPlayState()
      })
      mgSrc.onEnded(() => {
        this._isPlayState()
      })
      mgSrc.onStop(() => {
        this._isPlayState()
      })
    }
  }
})
