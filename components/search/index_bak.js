import {
  SearchModel
} from "../../models/search.js"
import {
  BookModel
} from "../../models/book.js"
let searchModel = new SearchModel()
let bookModel = new BookModel()
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    loadMore:{
      type: String,
      observer: 'loadMore'
    }
  },
 
  /**
   * 组件的初始数据
   */
  data: {
    searchVal: '',
    hotKeys: [],
    historyKeys: [],
    dataArray: [],
    noData: false,
    finished:false,
    lock: false, //翻页过快时可能会加载重复数据,加锁
    isCanLoad: false  //是否可加载下一页
  },
  //初始化会执行
  attached() {
    let hotVal = searchModel.getHotList()
    this.setData({
      historyKeys: searchModel.getHistoryList()
    })
    hotVal.then(res => {
      this.setData({
        hotKeys: res.hot
      })
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    onCancel() {
      this.initializationData()
      this.triggerEvent('cancel', {}, {})
    },
    onDelete(){
      this.initializationData()
      this.closeSearchLayer()
    },
    //搜索关键字回车
    onConfirm(event) {
      this.showSearchLayer()
      wx.showLoading()
      let keyword = event.detail.value || event.detail.text
      this.setData({
        searchVal: keyword,
        dataArray: [] //每次搜索完需清空上次存的列表数据
      })
      if (keyword) {
        bookModel.getSearch(keyword, 0).then(res => {
          wx.hideLoading()
          if(res.books.length){
            this.setData({
              dataArray: res.books
            })
          }else{
            this.setData({
              dataArray:[],
              noData: true //查询无数据 显示
            })
          }
          searchModel.setHistory(keyword) //将搜索关键字添加进缓存中
          this._initData() //每次搜索数据时，进行初始化
        })
      }
    },

    initializationData() {
      this.setData({
        dataArray: [],
        noData: false,
        lock: false
      })
    },
    _initData(){
      // this.data.lock = false
      this.data.isCanLoad = false
      //让滚动条移到最顶部
      // wx.pageScrollTo({
      //   scrollTop: 0
      // })
    },
    //加载更多，向下翻页
    loadMore(){
      let keyword = this.data.searchVal
      if (!keyword) return
      if (this._isLocked()) {
        return
      }
      if(!this.data.isCanLoad){
        this._locked() //请求前加锁
        bookModel.getSearch(keyword, this.data.dataArray.length).then(res => {
          if(res.count<20){
            this.data.isCanLoad = true
          }
          const tempArray = this.data.dataArray.concat(res.books)
          this.setData({
            dataArray: tempArray
          })
          this._unLocked()//请求成功后解锁
        },()=>{
          this._unLocked()//请求成功后解锁，比如在无网络的情况下恢复网络
        })
      }
    },
    showSearchLayer() {
      this.setData({
        finished: true
      })
    },
    closeSearchLayer() {
      this.setData({
        finished: false,
        searchVal: ''
      })
    },

    _isLocked() {
      return this.data.lock ? true : false
    },
    _locked() {
      this.setData({
        lock: true
      })
    },
    _unLocked() {
      this.setData({
        lock: false
      })
    }
  }
})