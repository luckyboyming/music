
let paginationBeh = Behavior({
  data: {
    dataArray: [],
    total: null,
    noData: false,
    lock: false, //翻页过快时可能会加载重复数据,加锁
  },
  methods: {
    setMoreData(dataArray){
      let tempArray = this.data.dataArray.concat(dataArray)
      this.setData({
        dataArray: tempArray
      })
    },
    getCurrentStart(){
      return this.data.dataArray.length
    },
    setTotal(total){
      this.data.total = total
      if(total == 0){
        this.setData({
          noData: true
        })
      }
    },
    hasMore(){
      if(this.data.dataArray.length >= this.data.total){
        return false
      }else{
        return true
      }
    },
    initializationData(){
      this.setData({
        dataArray: [], 
        noData: false,
        lock: false
      })
      this.data.total = null
    },
    isLocked() {
      return this.data.lock ? true : false
    },
    locked() {
      this.setData({
        lock: true
      })
    },
    unLocked() {
      this.setData({
        lock: false
      })
    }

  }
})

export {
  paginationBeh
}