// components/dateTime/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    index: {
      type: String,
      observer: function (news, olds, properties) {
        let val = news >= 10 ? news : "0" + news
        this.setData({
          _index: val
        })
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    _index: 0,
    year: 0,
    month: ''
  },
  attached: function () {
    let months = [
      '一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'
    ]
    
    let dateTime = new Date()
    let year = dateTime.getFullYear()
    let val = dateTime.getMonth()
    let month = months[val]
    this.setData({
      year,
      month
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {

  }
})
