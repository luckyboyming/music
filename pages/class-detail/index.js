import { ClassicModel } from "../../models/classic.js"
import { LikeModel } from "../../models/like.js"
let classicModel = new ClassicModel()
let likeModel = new LikeModel()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    classic: null,
    latest: true,
    first: false,
    like: false,
    count: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let cid = options.cid
    let type = options.type
    classicModel.getById(cid, type, (data) => {
      this._getLikeStatus(data.id, data.type)
      this.setData({
        classic: data
      })
    })
  },
  onLike: function (event) {
    console.log('////')
    let like_or_cancel = event.detail.typeClick
    console.log(this.data.classic.id, this.data.classic.type)
    likeModel.getLike(like_or_cancel, this.data.classic.id, this.data.classic.type)
  },
  _getLikeStatus(cid, type) {
    likeModel.getClassicLikeStatus(cid, type, (data) => {
      this.setData({
        like: data.like_status,
        count: data.fav_nums
      })
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})