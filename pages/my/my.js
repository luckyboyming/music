import { BookModel } from '../../models/book.js'
import { ClassicModel } from '../../models/classic.js'
let bookModel = new BookModel()
let classicModel = new ClassicModel()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hasUserInfo: false,
    userInfo: {},
    myBooksCount: 0,
    classics: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAuthorize()
    this.getMyBook()
    this.getMyFav()
  },
  getAuthorize(){
    wx.getSetting({
      success:res=>{
        if(res.authSetting['scope.userInfo']){
          wx.getUserInfo({
            success: res=>{
            //  console.log(res.userInfo)
              this.setData({
                hasUserInfo: true,
                userInfo:res.userInfo
              })
              wx.setStorage({
                key: 'userInfo',
                data: res.userInfo
              })
            }
          })
        }
      }
    })
  },
  onGetUserInfo(event){
    let userInfo = event.detail.userInfo
    if (userInfo){
      this.setData({
        userInfo,
        hasUserInfo: true
      })
    }
  },
  getMyFav(){
    classicModel.getMyFav(res=>{
      console.log(res)
      this.setData({
        classics:res
      })
    })
  },
  getMyBook(){
    bookModel.getMyBookCount().then(res=>{
      this.setData({
        myBooksCount: res.count
      })
    })
  },
  onJumpToAbout(){
    wx.navigateTo({
      url: '/pages/about/about'
    })
  },
  onPreviewTap(event){
    console.log(event)
    console.log(event.detail.cid)
    wx.navigateTo({
      url: `/pages/class-detail/index?cid=${event.detail.cid}&type=${event.detail.type}`
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getMyFav()
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getMyFav()
  }


})