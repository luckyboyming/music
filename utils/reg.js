const reg_ID_CARD = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, //身份证 //改用utils中的身份验证
  reg_PASSPORT = /(^[a-zA-Z]{5,17}$)|(^[a-zA-Z0-9]{5,17}$)/, //护照
  reg_MACAU_PASS = /^[HMhm]{1}([0-9]{10}|[0-9]{8})$/, //港澳通行证
  reg_TAIWAN_PASS = /(^[0-9]{8}$)|(^[0-9]{10}$)/, //台湾通行证
  reg_Tel = /^[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}$/,//手机号
  reg_validateNumber = new RegExp(/^\d{4}$/),//客户账户后四位
  reg_validateTrade = new RegExp(/^[\da-z]{6}$/i),//交易单号6位
  reg_Num = /^\d+$/,//检测正数字
  reg_passWord = /^[a-zA-Z0-9]{6,12}$/;//密码长度在6到12个字符
export {
  reg_ID_CARD,
  reg_PASSPORT,
  reg_MACAU_PASS,
  reg_TAIWAN_PASS,
  reg_Tel,
  reg_validateNumber,
  reg_validateTrade,
  reg_Num,
  reg_passWord
}