//let host ='https://dev.chuangchuang.cn';//测试环境
//let host = 'https://gray-crm.chuangchuang.cn';//灰度环境（线上）
//let host = 'https://sail-crm.chuangchuang.cn';//正式环境（线上）
var ajax = {
  /**
  * 请求数据
  // * @param type 请求类型
  // * @param url 请求地址
  // * @param param 请求传入的数据
  */
  // -----------------------登录-------------------------------------
  // 登录
  login(param){
    return api('POST','/scapi/worker/h5/admin/common/access', param, )
  }, 
  // 获取验证码
  getCode(mobile){
    return api('GET', `/scapi/interact/vCode/free/genVCode?mobile=${mobile}`)
  },
  // 通过手机验证码登录
  mobileLogin(param) {
    return api('POST', '/scapi/worker/h5/admin/mobile/access', param)
  },
  // 修改密码
  updatePassword(param) {
    return api('PUT', '/scapi/worker/worker4H5/updatePassword', param)
  },
  // -----------------------crm页面----------------------------------
  // 登陆获取用户菜单树
  userMenuTree() {
    return api('GET', `/scapi/worker/permission4H5/menuTree`)
  },
  // permissionChildMenuTree(id) {
  //   return api('GET', `/scapi/worker/permission4H5/childMenuTree/${id}`)
  // },
  // -----------------------首页----------------------------------
  //首页
  homePage() {
    return api('GET', `/scapi/interact/homePage4H5/todoList`)
  },
  // ---------------------------------------------订单模块---------------------------------------
  // ---------------------------------充值订单---------------------------------
  // 获取充值订单列表
  getRechargeList(param){
    return api('POST', '/scapi/finance/prepaidOrderH5/queryPage', param)
  },
  // 充值订单取消、作废订单
  prepaidOrderInvalid(id) {
    return api('GET',`/scapi/finance/prepaidOrderH5/invalid?id=${id}`)
  },
  // 获取充值订单详情
  rechargeOrderInfo(id) {
    return api('GET', `/scapi/finance/prepaidOrderH5/basicInformation?prepaidId=${id}`,)
  },
  // 申请开票
  applyInvoice(data) {
    return api('POST',`/scapi/finance/invoiceH5/insert`, data)
  },
  // 充值订单-下单客户列表
  rechargeCustomerList(name) {
    return api('GET', `/scapi/customer/companyPC/getListByCompanyName?companyName=${name}`)
  },
  // 新建充值订单
  addRechargeOrder(data) {
    return api('POST',`/scapi/finance/prepaidOrderH5/insert`, data)
  },
  // 充值订单回显
  rechargeOrderView(id) {
    return api('GET', `/scapi/finance/prepaidOrderPC/echo?id=${id}`)
  }, 
  // 企业工商信息
  companyDetailPC(id) {
    return api('GET', `/scapi/customer/companyPC/information?companyId=${id}`)
  },
  // 、、、、、、、、、、、、、、接口没调
  // 订单详情页-退款分页
  refundListInPerformDetail(data) {
    return api('POST',`/scapi/finance/refundPC/performQueryPage`, data)
  },
  // 订单详情页-红冲分页
  redHashedListInPerformDetail(data) {
    return api('POST',`/scapi/finance/redHashedPC/performQueryPage`, data)
  },
  // 调整用途分页
  adjustPage(data) {
    return api('POST',`/scapi/finance/adjustPC/prepaidQueryPage`, data)
  },
  // 订单详情-作废记录
  invalidPages(data) {
    return api('GET',`/scapi/finance/invalidRecordPC/detail?orderId=${data.orderId}&orderType=${data.orderType}`)
  },
  // 、、、、、、、、、、、、、、以上接口没调
  // ---------------------------------套餐订单---------------------------------
  // 套餐订单新建
  insertPackageOrder(data) {
    return api('POST',`/scapi/finance/packageOrderH5/insert`, data)
  },
  // 获取套餐订单列表
  getPackageList(param) {
    return api('POST', '/scapi/finance/packageOrderH5/queryPage', param)
  },
  // 套餐订单取消、作废订单
  packageOrderInvalid(id) {
    return api('GET', `/scapi/finance/packageOrderH5/invalid?packageOrderId=${id}`)
  },
  // 获取套餐订单详情
  packageOrderInfo(id) {
    return api('GET', `/scapi/finance/packageOrderH5/basicInformation?packageOrderId=${id}`)
  },
  // 新建套餐订单
  addpackageOrder(data) {
    return api('POST', `/scapi/finance/packageOrderH5/insert`, data)
  },
  // 套餐订单完款
  packageOrderAllgone(id) {
    return api('GET', `/scapi/finance/packageOrderH5/allGone?packageOrderId=${id}`)
  },

  //------------------ 执行订单--------------------------//
  // 执行订单 - 首页列表
  getExecuteList(data) {
    return api('POST', `/scapi/finance/performOrderH5/queryPageH5`, data)
  },
  // 执行订单 - 首页列表 - 取消订单
  cancelExecuteOrder(id) {
    return api('GET', `/scapi/finance/performOrderH5/invalid?performId=${id}`)
  },
  // 执行订单 - 首页列表 - 完款
  finishExecuteOrder(id) {
    return api('GET', `/scapi/finance/performOrderH5/allGone?performId=${id}`)
  },
  // 执行订单 - 新建服务 - 提交
  createServiceOrder(data) {
    return api('POST', `/scapi/finance/performOrderH5/insertServiceOrder`,data)
  },
  // ---------------------------------------------审批---------------------------------------
  //----------------操作审批------------------
  // 查看审批流程
  getAuditProcess(id){
    return api('GET', `/scapi/interact/flow4H5/detail/${id}`)
  },
  // 学员跳槽审批
  workerTransferdDetail(id) {
    return api('GET', `/scapi/customer/studentTracePC/approvalDetails?resourceId=${id}`)
  },
  // 客户转移审批
  customerTransferdDetail(id) {
    return api('GET', `/scapi/customer/companyTransferPC/approvalDetails?resourceId=${id}`)
  },
  // 开票审批
  invoiceApprovalDetail(id) {
    return api('GET', `/scapi/finance/invoicePC/approvalDetails?resourceId=${id}`)
  },
  // 合同审批
  approvalDetail(id) {
    return api('GET', `/scapi/finance/contract4H5/detail/${id}`)
  },
  // 执行订单审批
  executeDetail(id) {
    return api('GET', `/scapi/finance/performOrderPC/approvalDetails?resourceId=${id}`)
  },
  // 执行订单审批 - 订单分页
  executeOrderList(data) {
    return api('POST', `/scapi/finance/performOrderPC/pStudentQueryPage`,data)
  },
  // 调账审批详情
  adjustDetail(id) {
    return api('GET',`/scapi/finance/adjustPC/approvalDetails?adjustId=${id}`)
  },
   //企业更名审批
  renameOrderList(id){
    return api('GET', `/scapi/customer/companyRenamedPC/approvalDetails?resourceId=${id}`)
  },
  //红冲订单审批
  redHashOrderList(id) {
    return api('GET', `/scapi/finance/redHashedPC/approvalDetails?redHashedId=${id}`)
  },
  //套餐订单审批
  packageOrderList(id) {
    return api('GET', `/scapi/finance/packageOrderPC/approvalDetails?resourceId=${id}`)
  },

  // 执行订单审批 - 订单分页
  executeOrderList(data) {
    return api('POST', `/scapi/finance/performOrderPC/pStudentQueryPage`, data)
  },

  // 退款审批
  refundDetail(id) {
    return api('GET', `/scapi/finance/refundPC/approvalDetails?refundId=${id}`)
  },
  //操作审核
  getPageList(data){
    return api('POST', `/scapi/interact/flow4H5/queryPage`, data)
  },
  // 审批消息已读
  readAuditDetail(flowId) {
    return api('PUT',`/scapi/interact/flow4H5/read/${flowId}`)
  },
  // 通过不通过
  isPass(data) {
    return api('PUT',`/scapi/interact/flow4H5/handle`, data)
  },
  //撤销审批
  cancelApproval(id) {
    return api('PUT',`/scapi/interact/flow4H5/repeal/${id}`)
  },
  //执行，充值，套餐 撤消审批
  refundBackout(id) {
    return api('GET', `/scapi/finance/refundPC/invalid?refundId=${id}`)
  },

  // -----------------查看审批---------------
  //开票分页
  invoiceQueryPage(data) {
    return api('POST', `/scapi/finance/invoiceH5/queryPage`, data)
  },
  //审核管理-合同分页
  contractPage(data) {
    return api('POST', `/scapi/finance/contract4H5/queryPage`, data)
  },
  //审核管理-合同分页
  refundPage(data) {
    return api('POST', `/scapi/finance/refundH5/queryPage`, data)
  },
  //客户转移 分页
  transferList(data) {
    return api('POST', `/scapi/customer/companyTransferH5/queryPage`, data)
  },
  //企业更名 分页
  renameList(data) {
    return api('POST', `/scapi/customer/companyRenamedH5/queryPage`,data)
  },
  // 审核管理-合同确认-合同撤销
  contractCancel(id) {
    return api('PUT',`/scapi/finance/contract4H5/cancel/${id}`)
  },
  //审核管理-合同确认-提交审核
  submitAudit(id) {
    return api('PUT',`/scapi/finance/contract4PC/recheck/${id}`)
  },
  //审核管理-合同申请-发送短信
  showMessageInfo(id) {
    return api('GET',`/scapi/finance/contract4PC/getContactsInfo/${id}`)
  },
  sendMessage(id) {
    return api('PUT',`/scapi/finance/contract4PC/sendSignUrl/${id}`)
  },
 
  // -------------------审批详情----------------------------
  // 审批管理-开票详情-订单列表
  invoiceOrderPage(data) {
    return api('POST', `/scapi/finance/invoicePC/incomeQueryPage`, data)
  },
  //  ---------------------------------------------合同 ---------------------------------------------
  //主体列表
  tempCreateList() {
    return api('GET','/scapi/finance/contract4H5/allFirstParties')
  },
  // 查合同主体下模板列表
  contractTemList(id) {
    return api('GET',`/scapi/finance/contract4H5/contractTemplate/${id}`)
  },
  // 获取模板下合同渲染数据
  contractRegionList(templateId, companyId, totalMoney) {
    return api('GET', `/scapi/finance/contract4H5/contractRegionList/${templateId}?companyId=${companyId}&totalMoney=${totalMoney}`)
  },
  // 生成合同渲染预览
  contractDrawing(templateId, data) {
    return api('PUT', `/scapi/finance/contract4H5/drawing/${templateId}`,data,)
  },
  // 新增生成合同
  rechargeOrderCreateContract(data) {
    return api('POST', `/scapi/finance/contract4H5/add`, data,)
  },
  // ---------------------------------------------客户模块---------------------------------------
  /* —————————————————— 全部企业 —————————————————— */
  // 分页
  allfirmList(data) {
    return api('POST', `/scapi/customer/companyH5/queryPageHaveH5`, data)
  },
  //------------------ 课程表--------------------------
  // 课程表
  courseSchedule() {
    return api('GET', '/scapi/product/courseSchedule4H5/presentCourseSchedule')
  },
  //------------------ 准企业--------------------------
  // 准企业列表
  quasiFirmList(param) {
    return api('POST', '/scapi/customer/companyH5/queryPage', param,)
  },
  // 准企业新增时提示归属信息
  companyInsertPrompt(data) {
    return api('GET',`/scapi/customer/companyPC/insertPrompt?companyName=${data}`)
  },
  // 新增准企业
  addCompany(data) {
    return api('POST', '/scapi/customer/companyPC/insert', data)
  },
  // 准企业基本信息  客户公司公用的基本信息接口
  basicInfo(id) {
    return api('GET',`/scapi/customer/companyH5/basicInformation?companyId=${id}`)
  },
  // updateBasicInfo(data) {
  //   return axios.post(`/scapi/customer/companyPC/update`, data)
  // },
  // // 准企业主联系人信息
  // mainContactInfo(id) {
  //   return axios.get(`/scapi/customer/companyContactPC/getByCompanyId?companyId=${id}`)
  // },
  // updateContactInfo(data) {
  //   return axios.post(`/scapi/customer/companyContactPC/update`, data)
  // },
  // 客户联系人分页
  contactList(data) {
    return api('POST',`/scapi/customer/companyContactH5/queryPage`, data)
  },
  // 释放到公海
  releaseHighSeas(id) {
    return api('GET',`/scapi/customer/companyH5/releaseHighSeas?companyId=${id}`)
  },
  // 释放到无效库
  releaseInvalid(id) {
    return api('GET',`/scapi/customer/companyH5/releaseInvalid?companyId=${id}`)
  },
  // 回访记录列表
  visitList(data) {
    return api('POST',`/scapi/customer/returnVisitH5/queryPage`, data)
  },
  // 添加评论
  addComment(data) {
    return api('POST',`/scapi/customer/commentsH5/insert`, data)
  },
  
  // 获取分配记录
  assignInfo(id) {
    return api('GET', `/scapi/customer/companyH5/allotmentTrance?companyId=${id}`)
  },
  //银行账号
  bankAccount(data) {
    return api('GET', `/scapi/finance/bankPC/getList`, data)
  },
  // 设置主联系人
  setUpMain(id) {
    return api('GET',`/scapi/customer/companyContactH5/setUpMain?id=${id}`)
  },
  //------------------ 正式企业--------------------------
  // 添加回访记录
  addVisitList(data) {
    return api('POST', `/scapi/customer/returnVisitH5/insert`, data)
  },
  //添加企业联系人
  addContractPerson(data) {
    return api('POST', `/scapi/customer/companyContactH5/insert`, data)
  },
  //编辑回显企业联系人
  editShowContract(data) {
    return api('GET', `/scapi/customer/companyContactH5/echo?id=${data}`)
  },
  //修改企业联系人
  editContract(data) {
    return api('POST', `/scapi/customer/companyContactH5/update`, data)
  },
  //学员详情
  studentDetail(data) {
    return api('POST', `/scapi/customer/studentClassPC/companyQueryPage`, data)
  },
  //添加上课人员
  addStudentPerson(data) {
    return api('POST', `/scapi/customer/studentClassH5/insert`, data)
  },
  // 上课学员回显
  showMemberInfo(id) {
    return api('GET', `/scapi/customer/studentClassH5/echo?id=${id}`)
  },
  // 修改上课学员
  updateMemberInfo(data) {
    return api('POST', `/scapi/customer/studentClassH5/update`,data)
  },
  // 释放到公共库
  releasePublic(id) {
    return api('GET', `/scapi/customer/companyH5/release?companyId=${id}`)
  },
  // 转移销售
  transferSale(data) {
    return api('GET', `/scapi/customer/companyH5/transfer?companyId=${data.companyId}&note=${data.note}&nowEmployeesId=${data.nowEmployeesId}`)
  },
  // 搜索员工列表
  searchMemberList(data) {
    return api('POST', `/scapi/worker/worker4H5/conditionQueryPage`, data)
  },
  // 共享销售
  shareSale(data) {
    return api('GET', `/scapi/customer/companyShareH5/shareCompany?companyId=${data.companyId}&shareId=${data.shareId}`)
  },
  // 企业更名
  companyRename(data) {
    return api('POST', `/scapi/customer/companyRenamedH5/insert`, data)
  },
  //正式企业-新建课程订单
  createCourseList(data) {
    return api('POST', `/scapi/finance/performOrderH5/insertCourseOrder`, data)
  },
  //正式企业-选择上课学员
  getCourseStudent(data) {
    return api('GET', `/scapi/customer/studentClassPC/getSignList?companyId=${data.companyId}&isXinxun=${data.isXinxun}&productId=${data.productId}&studentName=${data.studentName}`)
  },
  // 正式企业-获取客户下优惠列表
  orderDiscountList(companyId, resourceId, resourceType, totalAmount) {
    return api('GET',`/scapi/finance/performOrderH5/getDIscountList?companyId=${companyId}&resourceId=${resourceId}&resourceType=${resourceType}&totalAmount=${totalAmount}`)
  },
  // 正式企业-获取客户下拉权益列表
  // getEquityList(companyId, productId, productType) {
  //   return api('GET', `/scapi/finance/interestsH5/getInterestList?companyId=${companyId}&productId=${productId}&productType=${productType}`)
  // },
  // 正式企业-获取客户下拉权益列表
  getEquityList(companyId, productId, productType) {
    return api('GET', `/scapi/finance/interestsPC/getInterestList?companyId=${companyId}&productId=${productId}&productType=${productType}`)
  },
  // 正式企业 - 新建代理订单
  submitAgentList(data) {
    return api('POST', `/scapi/finance/performOrderH5/insertAgency`, data)
  },
  // 正式企业 - 退费
  submitRefundList(data) {
    return api('POST', `/scapi/finance/refundH5/insert`, data)
  },
  // 正式企业 - 详情基本信息
  refundBasicInfo(id) {
    return api('GET', `/scapi/finance/performOrderH5/basicInformation?performId=${id}`)
  },
  // 正式企业 - 详情学员分页信息
  refundMemberInfo(data) {
    return api('POST', `/scapi/finance/performOrderH5/queryPageStudentH5`,data)
  },
  // 正式企业 - 电子账户详情
  accountDetail(data) {
    return api('POST', `/scapi/finance/accountH5/childQueryPageH5`, data)
  },
  // ---------------------------------------------------------全部学员---------------------------------
  // 全部学员分页
  allStudentList(data) {
    return api('POST', '/scapi/customer/studentClassH5/queryPage', data)
  },
  // 修改学员信息回显
  modifyStudent(id) {
    return api('GET', `/scapi/customer/studentClassH5/detailH5?id=${id}`)
  },

  // 学员跳槽申请 
  studentJump(note, nowCompanyId, studentId) {
    return api('GET', `/scapi/customer/studentClassH5/jobHopping?note=${note}&nowCompanyId=${nowCompanyId}&studentId=${studentId}`)
  },
  // 全部学员 - 学员详情 - 签到记录
  studentSignList(studentId) {
    return api('GET', `/scapi/customer/studentClassH5/getSignListH5?studentId=${studentId}`)
  },
  // 全部学员 - 学员详情 - 跳槽记录
  studentJumpList(data) {
    return api('POST', `/scapi/customer/studentTraceH5/queryPage`, data)
  },

  // ---------------------------------------------------------无效企业---------------------------------
  // 失效企业列表
  invalidPageList(param) {
    return api('POST', '/scapi/customer/invalidCompanyH5/queryPage', param)
  },
  // 无效客户-销售复活
  saleBack(id) {
    return api('GET',`/scapi/customer/invalidCompanyH5/resurrection?invalidId=${id}`)
  },
  // ---------------------------------------------------------准企业公海库---------------------------------
  //准企业公海库列表
  highSeasList(param) {
    return api('POST', '/scapi/customer/highSeasH5/queryPage', param)
  },
  // 准企业公海库领取
  receiveHighSeas(id) {
    return api('GET', `/scapi/customer/highSeasH5/receive?highSeasId=${id}`)
  },
  // ---------------------------------------------------------搜索列表/详情接口---------------------------------
  // 调用天眼查企业工商信息
  companyCheck(name) {
    return api('GET', `/scapi/customer/companyPC/companyCheck?companyName=${name}`)
  },
  // 搜索套餐列表-带价格
  packageSearchList(param) {
    return api('POST', '/scapi/product/package4H5/conditionDetailPage', param)
  },
  // 获取正式企业列表（精确搜索）
  getCompanyList(data) {
    return api('GET', `/scapi/customer/companyH5/accurate?companyName=${data}`)
  },
  // //获取正式企业列表（模糊搜索）
  // getAllCompanyList(data){
  //   return api('GET', `/scapi/customer/companyPC/fuzzyAll?companyName=${data}`)
  // },
  // 下单客户模糊查询下拉列表 正式企业下执行订单 套餐订单
  formalCustomerList(name) {
    return api('GET', `/scapi/customer/companyPC/fuzzy?companyName=${name}`)
  },
  // 客户下学员模糊搜索 新建套餐订单指定学员
  orderCustomerStudents(companyId, studentName) {
    return api('GET',`/scapi/customer/studentClassPC/getListByStudentName?companyId=${companyId}&studentName=${studentName}`)
  },
  //正式企业-执行订单-模糊搜索课程期
  getCourseClassList(data) {
    return api('POST', `/scapi/product/courseClass4H5/conditionQueryPage4Order`, data)
  },
  //正式企业-执行订单-按照编码查课程期
  getCourseClass(code) {
    return api('GET', `/scapi/product/courseClass4H5/listByCode/${code}`)
  },
  // 正式企业-执行订单-根据编号查代理订单
  getAgentList(id) {
    return api('GET', `/scapi/product/agency4H5/listByCode/${id}`)
  },
  // 正式企业-执行订单-根据编号模糊搜索服务服务订单
  getServiceSearchList(data) {
    return api('POST',`/scapi/product/service4H5/conditionQueryPage4Order`, data)
  },
  // 正式企业-执行订单-根据编号查服务订单
  getServiceList(id) {
    return api('GET', `/scapi/product/service4H5/listByCode/${id}`)
  },
  // 产品中心-套餐管理-详情
  packageDetail(id) {
    return api('GET', `/scapi/product/package4PC/detail/${id}`)
  },

}

export default ajax;
/**
 * 将小程序的API封装成支持Promise的API
 */
// 封装post请求
var  api = (method,url,data,config) => {
  var promise = new Promise((resolve, reject) => {
    //网络请求
    wx.request({
      url: getApp().globalData.ajaxHost +url,
      data: data,
      method: method,
      header: {
        // 'content-type': 'application/x-www-form-urlencoded',
        'Authorization': wx.getStorageSync('userInfo').token
      },
      success: function (res) {//服务器返回数据
      console.log(res,'sdsd3')
        if (res.statusCode == '401') {
          wx.showToast({
            title:res.data.message.global ? res.data.message.global : '登陆超时，请重新登陆！',
            icon: "none",
            mask: true
          })
          let pages = getCurrentPages();
          console.log(pages);
          if (pages[pages.length - 1].route =='pages/login/login') {
            
          }else{
            setTimeout(function () {
              wx.navigateTo({
                url: '/pages/login/login',
              })
            }, 2000)
          }
         
          
          return
        }
        if (res.statusCode == 200) {
          resolve(res.data);
        } else {//返回错误提示信息
          reject(res.data);
          console.log(res)
        }
      },
      error: function (e) {
        reject('网络出错');
        console.log('error')
      }
    })
  });
  return promise;
}
// * @params fn { Function } 小程序原始API，如wx.login
// function wxPromisify(fn) {
//   return function (obj = {}) {
//     return new Promise((resolve, reject) => {
//       obj.success = function (res) {
//         resolve(res)
//       }

//       obj.fail = function (res) {
//         reject(res)
//       }

//       fn(obj)
//     })
//   }
// }

// module.exports = {
//   wxPromisify: wxPromisify,
  
// }
// 使用方法
// var getLocationPromisified = util.wxPromisify(wx.getLocation)

// getLocationPromisified({
//   type: 'wgs84'
// }).then(function (res) {
//   var latitude = res.latitude
//   var longitude = res.longitude
//   var speed = res.speed
//   var accuracy = res.accuracy
// }).catch(function () {
//   console.error("get location failed")
// })