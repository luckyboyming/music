import { HTTP } from '../utils/http.js'

class ClassicModel extends HTTP {
  getFavLastest(callback) {
    this.request({
      url: '/classic/latest',
      success: res => {
        callback(res)
        this._setClassIndex(res.index) //将最新一期的index存缓存
      },
      fail: err => {
        callback(err)
      }
    })
  }
  handleChangePage(index,nextOrPrevious, callback) { //下一期
    this.request({
      url: `/classic/${index}/${nextOrPrevious}`,
      success: res => {
        callback(res)
      },
      fail: err => {
        callback(err)
      }
    })
  }

  getById(cid, type, success) {
    let params = {
      url: '/classic/' + type + '/' + cid,
      success: success
    }
    this.request(params)
  }

  _isFirst(index){
    return index === 1 ? true : false
  }
  _isLatest(index){
    let currentIndex = this._getCurrentIndex()
    console.log(currentIndex,index)
    return currentIndex === index ? true : false
  }
  getMyFav(callback){
    this.request({
      url: '/classic/favor',
      success: res=>{
        callback(res)
      }
    })
  }
  _setClassIndex(index){
    wx.setStorageSync('classicIndex', index)
  }
  _getCurrentIndex(){
    let index = wx.getStorageSync('classicIndex')
    return index
  }
}

export {
  ClassicModel
}