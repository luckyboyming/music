let myApp = getApp()
class TestModel {
  constructor() {
    this.baseUrl = myApp.globalData.testUrl
  }
  request({ url, data = {}, method = "GET" }) {
    return new Promise((resolve, reject) => {
      let newUrl = this.baseUrl + url
      wx.request({
        url: newUrl,
        data: data,
        method: method,
        header: {
          // 'content-type': 'application/json', // 默认值
          'Authorization': wx.getStorageSync('loginInfo').token
        },
        success: res => {
          let code = res.statusCode.toString()
          if (code.startsWith('2')) {
            resolve(res.data)
          } else if (code === '401') {
            wx.showToast({
              title: res.data.message.global ? res.data.message.global : '登陆超时，请重新登陆！',
              icon: "none",
              mask: true
            })
            wx.removeStorageSync('loginInfo')
            setTimeout(() => {
              wx.redirectTo({
                url: '/pages/login/index',
              })
            }, 2000)
          } else {
            reject()
          }
        },
        fail: err => {
          reject()
        }
      })
    })
  }
}

export {
  TestModel
}