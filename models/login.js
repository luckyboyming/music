var myApp = getApp()
import { TestModel } from '/test.js'

class CommonModel extends TestModel {
  super(){}
  login(username,password){
    return this.request({
      url: '/scapi/worker/h5/admin/common/access',
      method: 'POST',
      data: {
        password: password ,
        username: username
      }
    })
  }
  demoModel(){
    return this.request({
      url: '/scapi/interact/homePage4H5/todoList'
    })
  }

  
}

export {
  CommonModel
}